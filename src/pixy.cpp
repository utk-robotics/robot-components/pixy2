#include "rip/components/pixy.hpp"

// Local
#include "rip/exception_base.hpp"
#include "rip/logger.hpp"

namespace rip
{
#define BLOCK_BUFFER_SIZE 25

    NEW_RIP_EX(PixyInitException)
    NEW_RIP_EX(PixyBlocksException)

    Pixy::Pixy(const nlohmann::json& config)
    {
        // Configure white balance
        if(config.find("white_balance") != config.end())
        {
            // TODO evaluate auto vs manual
            /*
            nlohmann::json white_balance = config["white_balance"];
            if(white_balance.is_boolean())
            {
                pixy_cam_set_auto_white_balance(white_balance.get< bool >());
            }
            else if(white_balance.is_object())
            {
                if(white_balance.find("auto") != white_balance.end())
                {
                    pixy_cam_set_auto_white_balance(white_balance["auto"].get< bool >());
                }
                else
                {
                    uint8_t r, g, b;
                    try
                    {
                        r = white_balance["r"].get< uint8_t >();
                        g = white_balance["g"].get< uint8_t >();
                        b = white_balance["b"].get< uint8_t >();

                        pixy_cam_set_white_balance_value(r, g, b);
                    }
                    catch(...)
                    {
                    }
                }
            }
            else
            {
                throw ConfigException("Error: White Balance Config");
            }
            //*/
        }

        // Configure exposure
        /* TODO
        if(config.find("exposure") != config.end())
        {
            nlohmann::json exposure = config["exposure"];
            if(exposure.is_boolean())
            {
                pixy_cam_set_auto_exposure_compensation(exposure.get< bool >());
            }
            else if(exposure.is_object())
            {
                if(exposure.find("auto") != exposure.end())
                {
                    pixy_cam_set_auto_exposure_compensation(exposure["auto"].get< bool >());
                }
                else
                {
                    uint8_t gain;
                    uint16_t compensation;
                    try
                    {
                        gain         = exposure["gain"].get< uint8_t >();
                        compensation = exposure["compensation"].get< uint16_t >();

                        pixy_cam_set_exposure_compensation(gain, compensation);
                    }
                    catch(...)
                    {
                    }
                }
            }
            else
            {
                throw ConfigException("Error: Exposure Config");

            }
        }
        //*/

        // Configure brightness
        /*
        if(config.find("brightness") != config.end())
        {
            uint8_t brightness = config["brightness"].get< uint8_t >();
            pixy_cam_set_auto_white_balance(brightness);
        }
        //*/
    }

    void Pixy::start()
    {
        int status = m_pixy.init();
        if (status < 0)
        {
            throw PixyInitException("Pixy2 Init returned %d", status);
        }
        else
        {
            Logger::info("Pixy2 Connection established.");
        }

        m_thread = std::make_unique< std::thread >(&Pixy::run, this);
    }

    void Pixy::stop()
    {
        if(m_running)
        {
            m_running = false;
            m_thread->join();
        }

        // pixy_close();
    }

    void Pixy::addCallback(const std::function< void(std::vector< Block >) >& callback)
    {
        m_callback.permanentBind(callback);
    }

    std::vector< Block > Pixy::get()
    {
        std::lock_guard< std::mutex > lock(m_mutex);
        return m_last;
    }

    void Pixy::run()
    {
        m_running = true;

        struct Block blocks[BLOCK_BUFFER_SIZE];
        int num_blocks;

        while(m_running)
        {
            // Wait for new blocks to be available
            while( !m_pixy.ccc.numBlocks && m_running)
            {
                m_pixy.ccc.getBlocks();
            }

            // num_blocks = pixy_get_blocks(BLOCK_BUFFER_SIZE, &blocks[0]);
            num_blocks = m_pixy.ccc.numBlocks;
            if(num_blocks < 0)
            {
                throw PixyBlocksException();
            }

            m_mutex.lock();
            m_last.resize(num_blocks);
            for (int Block_Index = 0; Block_Index < m_pixy.ccc.numBlocks; ++Block_Index)
            {
              // printf ("  Block %d: ", Block_Index + 1);
              // pixy.ccc.blocks[Block_Index].print();
              m_last[Block_Index] = m_pixy.ccc.blocks[Block_Index];
            }

            m_mutex.unlock();
            m_callback.trigger(m_last);
        }

        delete[] blocks;
    }
}  // namespace rip
