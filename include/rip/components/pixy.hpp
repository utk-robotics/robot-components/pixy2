#ifndef PIXY_COMPONENT_HPP
#define PIXY_COMPONENT_HPP

// Global
#include <memory>
#include <mutex>
#include <set>
#include <thread>
#include <vector>

// External
#include <libpixyusb2.h>

#include <nlohmann/json.hpp>

// Local
#include "rip/callback.hpp"

namespace rip
{
    /**
     * @class Pixy
     * @brief Wrapper for the CMU Pixy USB library
     */
    class Pixy
    {
    public:
        /**
         * @brief Constructor
         * @param config The configurations for white balance, exposure, and
         * brightness
         */
        Pixy(const nlohmann::json& config);

        /**
         * @brief Start the camera and a background thread to read from it
         */
        void start();

        /**
         * @brief Stop the camera and the background thread reading from it
         */
        void stop();

        /**
         * @brief Adds a callback function that is triggered when new
         * information is received from the camera.
         *
         * @param callback The callback function to add
         */
        void addCallback(const std::function< void(std::vector< Block >) >& callback);

        /**
         * @brief Returns the last set of information received from the camera
         *
         * @return the last set of information received from the camera
         */
        std::vector< Block > get();

    private:
        Pixy2 m_pixy;

        /**
         * @brief The function for the background thread receiving information
         * from the camera
         */
        void run();

        bool m_running;
        std::unique_ptr< std::thread > m_thread;
        std::mutex m_mutex;

        Callback< std::vector< Block > > m_callback;
        std::vector< Block > m_last;
    };
}  // namespace rip

#endif  // PIXY_COMPONENT_HPP
